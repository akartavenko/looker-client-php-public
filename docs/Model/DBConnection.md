# DBConnection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the connection. Also used as the unique identifier | [optional] 
**host** | **string** | Host name/address of server | [optional] 
**port** | **string** | Port number on server | [optional] 
**username** | **string** | Username for server authentication | [optional] 
**password** | **string** | (Write-only) Password for server authentication | [optional] 
**certificate** | **string** | (Write-only) Base64 encoded Certificate body for server authentication (when appropriate for dialect). | [optional] 
**database** | **string** | Database name | [optional] 
**db_timezone** | **string** | Time zone of database | [optional] 
**query_timezone** | **string** | Timezone to use in queries | [optional] 
**schema** | **string** | Scheme name | [optional] 
**max_connections** | **int** | Maximum number of concurrent connection to use | [optional] 
**ssl** | **bool** | Use SSL/TLS when connecting to server | [optional] 
**verify_ssl** | **bool** | Verify the SSL | [optional] 
**tmp_db_name** | **string** | Name of temporary database (if used) | [optional] 
**jdbc_additional_params** | **string** | Additional params to add to JDBC connection string | [optional] 
**pool_timeout** | **int** | Pool Timeout | [optional] 
**dialect** | [**\Swagger\Client\Model\Dialect**](Dialect.md) | (Read-only) SQL Dialect details | [optional] 
**dialect_name** | **string** | (Read/Write) SQL Dialect name | [optional] 
**snippets** | [**\Swagger\Client\Model\Snippet[]**](Snippet.md) | SQL Runner snippets for this connection | [optional] 
**created_at** | **string** | Creation date for this connection | [optional] 
**user_id** | **string** | Id of user who last modified this connection configuration | [optional] 
**example** | **bool** | Is this an example connection | [optional] 
**user_db_credentials** | **bool** | (Limited access feature) Are per user db credentials enabled | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


