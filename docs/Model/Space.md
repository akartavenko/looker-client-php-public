# Space

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique Id | [optional] 
**content_metadata_id** | **int** | Id of content metadata | [optional] 
**creator_id** | **int** | User Id of Creator | [optional] 
**name** | **string** | Unique Name | [optional] 
**is_personal** | **bool** | Space is a user&#39;s personal space | [optional] 
**is_root** | **bool** | Space is the root shared space | [optional] 
**is_user_root** | **bool** | Space is the root user space | [optional] 
**parent_id** | **int** | (Write-only) Id of Parent | [optional] 
**looks** | [**\Swagger\Client\Model\LookWithDashboards[]**](LookWithDashboards.md) | Looks | [optional] 
**dashboards** | [**\Swagger\Client\Model\DashboardBase[]**](DashboardBase.md) | Dashboards | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


