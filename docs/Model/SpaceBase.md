# SpaceBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique Id | [optional] 
**content_metadata_id** | **int** | Id of content metadata | [optional] 
**creator_id** | **int** | User Id of Creator | [optional] 
**name** | **string** | Unique Name | [optional] 
**is_personal** | **bool** | Space is a user&#39;s personal space | [optional] 
**is_root** | **bool** | Space is the root shared space | [optional] 
**is_user_root** | **bool** | Space is the root user space | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


