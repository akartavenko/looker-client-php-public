# Query

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**model** | **string** | Model | [optional] 
**view** | **string** | View | [optional] 
**fields** | **string[]** | Fields | [optional] 
**pivots** | **string[]** | Pivots | [optional] 
**filters** | **map[string,string]** | Filters | [optional] 
**filter_expression** | **string** | Filter Expression | [optional] 
**sorts** | **string[]** | Sorts | [optional] 
**limit** | **string** | Limit | [optional] 
**column_limit** | **string** | Column Limit | [optional] 
**total** | **bool** | Total | [optional] 
**row_total** | **string** | Raw Total | [optional] 
**runtime** | **double** | Runtime | [optional] 
**vis_config** | **map[string,string]** | Visualization Config | [optional] 
**filter_config** | **map[string,string]** | Filter Config | [optional] 
**visible_ui_sections** | **string** | Visible UI Sections | [optional] 
**slug** | **string** | Slug | [optional] 
**dynamic_fields** | **string[]** | Dynamic Fields | [optional] 
**client_id** | **string** | Client Id | [optional] 
**share_url** | **string** | Share Url | [optional] 
**expanded_share_url** | **string** | Expanded Share Url | [optional] 
**query_timezone** | **string** | Query Timezone | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


