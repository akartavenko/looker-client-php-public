# DashboardElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**dashboard_id** | **string** | Id of Dashboard | [optional] 
**look_id** | **int** | Id Of Look | [optional] 
**type** | **string** | Type | [optional] 
**listen** | **string** | Listen | [optional] 
**refresh_interval** | **string** | Refresh Interval | [optional] 
**refresh_interval_to_i** | **int** | Refresh Interval as integer | [optional] 
**note_text** | **string** | Note Text | [optional] 
**note_text_as_html** | **string** | Note Text as Html | [optional] 
**note_display** | **string** | Note Display | [optional] 
**note_state** | **string** | Note State | [optional] 
**title_text** | **string** | Text tile title text | [optional] 
**subtitle_text** | **string** | Text tile subtitle text | [optional] 
**body_text** | **string** | Text tile body text | [optional] 
**body_text_as_html** | **string** | Text tile body text as Html | [optional] 
**look** | [**\Swagger\Client\Model\LookWithQuery**](LookWithQuery.md) | Look | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


