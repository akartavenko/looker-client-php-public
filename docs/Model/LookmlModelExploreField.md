# LookmlModelExploreField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name | [optional] 
**description** | **string** | Description | [optional] 
**label** | **string** | Label | [optional] 
**label_short** | **string** | Short version of label | [optional] 
**view_label** | **string** | Label of view | [optional] 
**align** | **string** | Alignment type | [optional] 
**default_filter_value** | **string** | Default filter value | [optional] 
**hidden** | **bool** | Is hidden | [optional] 
**view** | **string** | View name | [optional] 
**type** | **string** | Type of field | [optional] 
**is_numeric** | **bool** | Is numeric | [optional] 
**scope** | **string** | Scope | [optional] 
**project_name** | **string** | Project name | [optional] 
**source_file** | **string** | Source file | [optional] 
**can_filter** | **bool** | Can filter | [optional] 
**requires_refresh_on_sort** | **bool** | Requires refres on sort | [optional] 
**sortable** | **bool** | Is sortable | [optional] 
**value_format** | **string** | Value format | [optional] 
**field_group_label** | **string** | Field group label | [optional] 
**field_group_variant** | **string** | Field Group variant | [optional] 
**suggest_explore** | **string** | Explore for suggest | [optional] 
**suggest_dimension** | **string** | Dimension for suggest | [optional] 
**enumerations** | [**\Swagger\Client\Model\LookmlModelExploreFieldEnumeration[]**](LookmlModelExploreFieldEnumeration.md) | Array of enumerations | [optional] 
**suggestable** | **bool** | Is suggestable | [optional] 
**sql** | **string** | SQL expression | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


