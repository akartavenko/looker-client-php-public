# DashboardLayout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**dashboard_id** | **string** | Id of Dashboard | [optional] 
**type** | **string** | Type | [optional] 
**active** | **bool** | Is Active | [optional] 
**column_width** | **int** | Column Width | [optional] 
**width** | **int** | Width | [optional] 
**components** | [**\Swagger\Client\Model\DashboardLayoutComponent[]**](DashboardLayoutComponent.md) | Components | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


