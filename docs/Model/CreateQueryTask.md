# CreateQueryTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query_id** | **int** | Id of query to run | 
**result_format** | **string** | Desired result format | [optional] 
**source** | **string** | Source of query task | [optional] 
**deferred** | **bool** | Create the task but defer execution | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


