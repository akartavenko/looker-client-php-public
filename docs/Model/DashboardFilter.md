# DashboardFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**name** | **string** | Name of filter | [optional] 
**title** | **string** | Name of title | [optional] 
**type** | **string** | Type of filter: one of date, number, string, or field | [optional] 
**default_value** | **string** | Default value of filter | [optional] 
**model** | **string** | Model of filter (required if type &#x3D; field) | [optional] 
**explore** | **string** | Explore of filter (required if type &#x3D; field) | [optional] 
**dimension** | **string** | Dimension of filter (required if type &#x3D; field) | [optional] 
**field** | **map[string,string]** | Field information | [optional] 
**listens_to_filters** | **string[]** | Array of listeners for faceted filters | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


