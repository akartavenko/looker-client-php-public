# ScheduledPlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**name** | **string** | Name | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Date and time when ScheduledPlan was created | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Date and time when ScheduledPlan was last updated | [optional] 
**title** | **string** | Title | [optional] 
**user_id** | **int** | User Id which owns this ScheduledPlan | [optional] 
**user** | [**\Swagger\Client\Model\UserPublic**](UserPublic.md) | User who owns this ScheduledPlan | [optional] 
**enabled** | **bool** | Whether the ScheduledPlan is enabled | [optional] 
**next_run_at** | [**\DateTime**](\DateTime.md) | When the ScheduledPlan will next run (null if running once) | [optional] 
**last_run_at** | [**\DateTime**](\DateTime.md) | When the ScheduledPlan was last run | [optional] 
**look_id** | **int** | Id of a look | [optional] 
**dashboard_id** | **int** | Id of a dashboard | [optional] 
**lookml_dashboard_id** | **string** | Id of a LookML dashboard | [optional] 
**require_results** | **bool** | Delivery should occur if running the dashboard or look returns results | [optional] 
**require_no_results** | **bool** | Delivery should occur if the dashboard look does not return results | [optional] 
**require_change** | **bool** | Delivery should occur if data have changed since the last run | [optional] 
**crontab** | **string** | Vixie-Style crontab specification when to run | [optional] 
**timezone** | **string** | Timezone for interpreting the specified crontab (default is Looker instance timezone) | [optional] 
**scheduled_plan_destination** | [**\Swagger\Client\Model\ScheduledPlanDestination[]**](ScheduledPlanDestination.md) | Scheduled plan destinations | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


