# LegacyFeature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique Id | [optional] 
**name** | **string** | Name | [optional] 
**description** | **string** | Description | [optional] 
**enabled_locally** | **bool** | Whether this feature has been enabled by a user | [optional] 
**enabled** | **bool** | Whether this feature is currently enabled | [optional] 
**disallowed_as_of_version** | **string** | Looker version where this feature became a legacy feature | [optional] 
**end_of_life_version** | **string** | Future Looker version where this feature will be removed | [optional] 
**documentation_url** | **string** | URL for documentation about this feature | [optional] 
**url** | **string** | Link to get this item | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


