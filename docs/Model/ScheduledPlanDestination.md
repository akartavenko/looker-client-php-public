# ScheduledPlanDestination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**scheduled_plan_id** | **int** | Id of a scheduled plan you own | [optional] 
**format** | **string** | Format requested by the given destination (i.e. PDF, etc.) | [optional] 
**apply_formatting** | **bool** | Are values formatted? (containing currency symbols, digit separators, etc. | [optional] 
**address** | **string** | Address for recipient (only email addresses supported for now) | [optional] 
**type** | **string** | Type of the address (only &#39;email&#39; is supported for now) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


