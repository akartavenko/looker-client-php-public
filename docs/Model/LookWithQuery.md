# LookWithQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique Id | [optional] 
**content_metadata_id** | **int** | Id of content metadata | [optional] 
**title** | **string** | Look Title | [optional] 
**user** | [**\Swagger\Client\Model\UserIdOnly**](UserIdOnly.md) | User | [optional] 
**query_id** | **int** | Query Id | [optional] 
**description** | **string** | Description | [optional] 
**short_url** | **string** | Short Url | [optional] 
**space** | [**\Swagger\Client\Model\SpaceBase**](SpaceBase.md) | Space of this Look | [optional] 
**public** | **bool** | Is Public | [optional] 
**public_slug** | **string** | Public Slug | [optional] 
**user_id** | **int** | (Write-only) User Id | [optional] 
**space_id** | **string** | (Write-only) Space Id | [optional] 
**model** | [**\Swagger\Client\Model\LookModel**](LookModel.md) | Model | [optional] 
**public_url** | **string** | Public Url | [optional] 
**embed_url** | **string** | Embed Url | [optional] 
**google_spreadsheet_formula** | **string** | Google Spreadsheet Formula | [optional] 
**excel_file_url** | **string** | Excel File Url | [optional] 
**url** | **string** | Url | [optional] 
**query** | [**\Swagger\Client\Model\Query**](Query.md) | Query | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Time that the Look was created. | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Time that the Look was updated. | [optional] 
**last_updater_id** | **int** | (Write-only) Id of User that last updated the look. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


