# LookMovePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**looks_to_move** | [**\Swagger\Client\Model\LookBasic[]**](LookBasic.md) |  | [optional] 
**looks_to_copy** | [**\Swagger\Client\Model\LookBasic[]**](LookBasic.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


