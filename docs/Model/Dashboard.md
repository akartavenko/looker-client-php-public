# Dashboard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique Id | [optional] 
**content_metadata_id** | **int** | Id of content metadata | [optional] 
**user_id** | **int** | Id of User | [optional] 
**title** | **string** | Look Title | [optional] 
**description** | **string** | Description | [optional] 
**readonly** | **bool** | Is Read-only | [optional] 
**hidden** | **bool** | Is Hidden | [optional] 
**refresh_interval** | **string** | Refresh Interval | [optional] 
**refresh_interval_to_i** | **int** | Refresh Interval as Integer | [optional] 
**space** | [**\Swagger\Client\Model\SpaceBase**](SpaceBase.md) | Space | [optional] 
**load_configuration** | **string** | configuration option that governs how dashboard loading will happen. | [optional] 
**model** | [**\Swagger\Client\Model\LookModel**](LookModel.md) | Model | [optional] 
**space_id** | **string** | Id of Space | [optional] 
**elements** | [**\Swagger\Client\Model\DashboardElement[]**](DashboardElement.md) | Elements | [optional] 
**layouts** | [**\Swagger\Client\Model\DashboardLayout[]**](DashboardLayout.md) | Layouts | [optional] 
**filters** | [**\Swagger\Client\Model\DashboardFilter[]**](DashboardFilter.md) | Filters | [optional] 
**background_color** | **string** | Background color | [optional] 
**show_title** | **bool** | Show title | [optional] 
**title_color** | **string** | Title color | [optional] 
**show_filters_bar** | **bool** | Show filters bar | [optional] 
**tile_background_color** | **string** | Tile background color | [optional] 
**tile_text_color** | **string** | Tile text color | [optional] 
**text_tile_text_color** | **string** | Color of text on text tiles | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


