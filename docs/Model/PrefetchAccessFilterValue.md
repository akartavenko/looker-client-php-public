# PrefetchAccessFilterValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **string** | Access filter model name. | [optional] 
**field** | **string** | Access filter field name. | [optional] 
**value** | **string** | Access filter value | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


