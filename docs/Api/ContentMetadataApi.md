# Swagger\Client\ContentMetadataApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**allContentMetadataAccesss**](ContentMetadataApi.md#allContentMetadataAccesss) | **GET** /content_metadata_access | Get All content_metadata_accesss
[**allContentMetadatas**](ContentMetadataApi.md#allContentMetadatas) | **GET** /content_metadata | Get All content_metadatas
[**contentMetadata**](ContentMetadataApi.md#contentMetadata) | **GET** /content_metadata/{content_metadata_id} | Get content_metadata
[**createContentMetadataAccess**](ContentMetadataApi.md#createContentMetadataAccess) | **POST** /content_metadata_access | Create content_metadata_access
[**deleteContentMetadataAccess**](ContentMetadataApi.md#deleteContentMetadataAccess) | **DELETE** /content_metadata_access/{content_metadata_access_id} | Delete content_metadata_access
[**updateContentMetadata**](ContentMetadataApi.md#updateContentMetadata) | **PATCH** /content_metadata/{content_metadata_id} | Update content_metadata
[**updateContentMetadataAccess**](ContentMetadataApi.md#updateContentMetadataAccess) | **PUT** /content_metadata_access/{content_metadata_access_id} | Update content_metadata_access


# **allContentMetadataAccesss**
> \Swagger\Client\Model\ContentMetaGroupUser[] allContentMetadataAccesss($content_metadata_id, $fields)

Get All content_metadata_accesss

### All content metadata access records for content metadata.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$content_metadata_id = 789; // int | Id of content metadata
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allContentMetadataAccesss($content_metadata_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->allContentMetadataAccesss: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_metadata_id** | **int**| Id of content metadata | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\ContentMetaGroupUser[]**](../Model/ContentMetaGroupUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allContentMetadatas**
> \Swagger\Client\Model\ContentMeta[] allContentMetadatas($fields, $parent_id)

Get All content_metadatas

### Get information about all content metadata in a space.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$fields = "fields_example"; // string | Requested fields.
$parent_id = 789; // int | Parent space of content.

try {
    $result = $api_instance->allContentMetadatas($fields, $parent_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->allContentMetadatas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fields. | [optional]
 **parent_id** | **int**| Parent space of content. | [optional]

### Return type

[**\Swagger\Client\Model\ContentMeta[]**](../Model/ContentMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contentMetadata**
> \Swagger\Client\Model\ContentMeta contentMetadata($content_metadata_id, $fields)

Get content_metadata

### Get information about an individual content metadata record.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$content_metadata_id = 789; // int | Id of content metadata
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->contentMetadata($content_metadata_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->contentMetadata: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_metadata_id** | **int**| Id of content metadata |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\ContentMeta**](../Model/ContentMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createContentMetadataAccess**
> \Swagger\Client\Model\ContentMetaGroupUser createContentMetadataAccess($body)

Create content_metadata_access

### Create content metadata access.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$body = new \Swagger\Client\Model\ContentMetaGroupUser(); // \Swagger\Client\Model\ContentMetaGroupUser | content_metadata_access

try {
    $result = $api_instance->createContentMetadataAccess($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->createContentMetadataAccess: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ContentMetaGroupUser**](../Model/\Swagger\Client\Model\ContentMetaGroupUser.md)| content_metadata_access | [optional]

### Return type

[**\Swagger\Client\Model\ContentMetaGroupUser**](../Model/ContentMetaGroupUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteContentMetadataAccess**
> string deleteContentMetadataAccess($content_metadata_access_id)

Delete content_metadata_access

### Remove content metadata access.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$content_metadata_access_id = 789; // int | Id of content metadata access

try {
    $result = $api_instance->deleteContentMetadataAccess($content_metadata_access_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->deleteContentMetadataAccess: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_metadata_access_id** | **int**| Id of content metadata access |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateContentMetadata**
> \Swagger\Client\Model\ContentMeta updateContentMetadata($content_metadata_id, $body)

Update content_metadata

### Move a piece of content.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$content_metadata_id = 789; // int | Id of content metadata
$body = new \Swagger\Client\Model\ContentMeta(); // \Swagger\Client\Model\ContentMeta | content_metadata

try {
    $result = $api_instance->updateContentMetadata($content_metadata_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->updateContentMetadata: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_metadata_id** | **int**| Id of content metadata |
 **body** | [**\Swagger\Client\Model\ContentMeta**](../Model/\Swagger\Client\Model\ContentMeta.md)| content_metadata |

### Return type

[**\Swagger\Client\Model\ContentMeta**](../Model/ContentMeta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateContentMetadataAccess**
> \Swagger\Client\Model\ContentMetaGroupUser updateContentMetadataAccess($content_metadata_access_id, $body)

Update content_metadata_access

### Update type of access for content metadata.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ContentMetadataApi();
$content_metadata_access_id = 789; // int | Id of content metadata access
$body = new \Swagger\Client\Model\ContentMetaGroupUser(); // \Swagger\Client\Model\ContentMetaGroupUser | content_metadata_access

try {
    $result = $api_instance->updateContentMetadataAccess($content_metadata_access_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContentMetadataApi->updateContentMetadataAccess: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content_metadata_access_id** | **int**| Id of content metadata access |
 **body** | [**\Swagger\Client\Model\ContentMetaGroupUser**](../Model/\Swagger\Client\Model\ContentMetaGroupUser.md)| content_metadata_access |

### Return type

[**\Swagger\Client\Model\ContentMetaGroupUser**](../Model/ContentMetaGroupUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

