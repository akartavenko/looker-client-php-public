# Swagger\Client\RunningQueriesApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**allRunningQueries**](RunningQueriesApi.md#allRunningQueries) | **GET** /running_queries | Get All Running Queries
[**killQuery**](RunningQueriesApi.md#killQuery) | **DELETE** /running_queries/{query_slug} | Kill Running Query


# **allRunningQueries**
> \Swagger\Client\Model\RunningQueries[] allRunningQueries()

Get All Running Queries

Get information about all running queries.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\RunningQueriesApi();

try {
    $result = $api_instance->allRunningQueries();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RunningQueriesApi->allRunningQueries: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\RunningQueries[]**](../Model/RunningQueries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **killQuery**
> string killQuery($query_slug)

Kill Running Query

Kill a query with a specific slug.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\RunningQueriesApi();
$query_slug = "query_slug_example"; // string | query slug

try {
    $result = $api_instance->killQuery($query_slug);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RunningQueriesApi->killQuery: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query_slug** | **string**| query slug |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

