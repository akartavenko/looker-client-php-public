# Swagger\Client\UserApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**allUserAccessFilters**](UserApi.md#allUserAccessFilters) | **GET** /users/{user_id}/access_filters | Get All Access Filters
[**allUserCredentialsApi3s**](UserApi.md#allUserCredentialsApi3s) | **GET** /users/{user_id}/credentials_api3 | Get All API 3 Credentials
[**allUserCredentialsEmbeds**](UserApi.md#allUserCredentialsEmbeds) | **GET** /users/{user_id}/credentials_embed | Get All Embedding Credentials
[**allUserSessions**](UserApi.md#allUserSessions) | **GET** /users/{user_id}/sessions | Get All Web Login Sessions
[**allUsers**](UserApi.md#allUsers) | **GET** /users | Get All Users
[**createUser**](UserApi.md#createUser) | **POST** /users | Create User
[**createUserAccessFilter**](UserApi.md#createUserAccessFilter) | **POST** /users/{user_id}/access_filters | Create Access Filter
[**createUserCredentialsApi**](UserApi.md#createUserCredentialsApi) | **POST** /users/{user_id}/credentials_api | Create API Credential
[**createUserCredentialsApi3**](UserApi.md#createUserCredentialsApi3) | **POST** /users/{user_id}/credentials_api3 | Create API 3 Credential
[**createUserCredentialsEmail**](UserApi.md#createUserCredentialsEmail) | **POST** /users/{user_id}/credentials_email | Create Email/Password Credential
[**createUserCredentialsEmailPasswordReset**](UserApi.md#createUserCredentialsEmailPasswordReset) | **POST** /users/{user_id}/credentials_email/password_reset | Create Password Reset Token
[**createUserCredentialsTotp**](UserApi.md#createUserCredentialsTotp) | **POST** /users/{user_id}/credentials_totp | Create Two-Factor Credential
[**deleteUser**](UserApi.md#deleteUser) | **DELETE** /users/{user_id} | Delete User
[**deleteUserAccessFilter**](UserApi.md#deleteUserAccessFilter) | **DELETE** /users/{user_id}/access_filters/{access_filter_id} | Delete Access Filter
[**deleteUserCredentialsApi**](UserApi.md#deleteUserCredentialsApi) | **DELETE** /users/{user_id}/credentials_api | Delete API Credential
[**deleteUserCredentialsApi3**](UserApi.md#deleteUserCredentialsApi3) | **DELETE** /users/{user_id}/credentials_api3/{credentials_api3_id} | Delete API 3 Credential
[**deleteUserCredentialsEmail**](UserApi.md#deleteUserCredentialsEmail) | **DELETE** /users/{user_id}/credentials_email | Delete Email/Password Credential
[**deleteUserCredentialsEmbed**](UserApi.md#deleteUserCredentialsEmbed) | **DELETE** /users/{user_id}/credentials_embed/{credentials_embed_id} | Delete Embedding Credential
[**deleteUserCredentialsGoogle**](UserApi.md#deleteUserCredentialsGoogle) | **DELETE** /users/{user_id}/credentials_google | Delete Google Auth Credential
[**deleteUserCredentialsLdap**](UserApi.md#deleteUserCredentialsLdap) | **DELETE** /users/{user_id}/credentials_ldap | Delete LDAP Credential
[**deleteUserCredentialsLookerOpenid**](UserApi.md#deleteUserCredentialsLookerOpenid) | **DELETE** /users/{user_id}/credentials_looker_openid | Delete Looker OpenId Credential
[**deleteUserCredentialsSaml**](UserApi.md#deleteUserCredentialsSaml) | **DELETE** /users/{user_id}/credentials_saml | Delete Saml Auth Credential
[**deleteUserCredentialsTotp**](UserApi.md#deleteUserCredentialsTotp) | **DELETE** /users/{user_id}/credentials_totp | Delete Two-Factor Credential
[**deleteUserSession**](UserApi.md#deleteUserSession) | **DELETE** /users/{user_id}/sessions/{session_id} | Delete Web Login Session
[**me**](UserApi.md#me) | **GET** /user | Get Current User
[**searchUsers**](UserApi.md#searchUsers) | **GET** /users/search | Search Users
[**searchUsersNames**](UserApi.md#searchUsersNames) | **GET** /users/search/names/{pattern} | Search User Names
[**setUserRoles**](UserApi.md#setUserRoles) | **PUT** /users/{user_id}/roles | Set User Roles
[**updateUser**](UserApi.md#updateUser) | **PATCH** /users/{user_id} | Update User
[**updateUserAccessFilter**](UserApi.md#updateUserAccessFilter) | **PATCH** /users/{user_id}/access_filters/{access_filter_id} | Update Access Filter
[**updateUserCredentialsEmail**](UserApi.md#updateUserCredentialsEmail) | **PATCH** /users/{user_id}/credentials_email | Update Email/Password Credential
[**user**](UserApi.md#user) | **GET** /users/{user_id} | Get User by Id
[**userAccessFilter**](UserApi.md#userAccessFilter) | **GET** /users/{user_id}/access_filters/{access_filter_id} | Get Access Filter
[**userCredentialsApi**](UserApi.md#userCredentialsApi) | **GET** /users/{user_id}/credentials_api | Get API Credential
[**userCredentialsApi3**](UserApi.md#userCredentialsApi3) | **GET** /users/{user_id}/credentials_api3/{credentials_api3_id} | Get API 3 Credential
[**userCredentialsEmail**](UserApi.md#userCredentialsEmail) | **GET** /users/{user_id}/credentials_email | Get Email/Password Credential
[**userCredentialsEmbed**](UserApi.md#userCredentialsEmbed) | **GET** /users/{user_id}/credentials_embed/{credentials_embed_id} | Get Embedding Credential
[**userCredentialsGoogle**](UserApi.md#userCredentialsGoogle) | **GET** /users/{user_id}/credentials_google | Get Google Auth Credential
[**userCredentialsLdap**](UserApi.md#userCredentialsLdap) | **GET** /users/{user_id}/credentials_ldap | Get LDAP Credential
[**userCredentialsLookerOpenid**](UserApi.md#userCredentialsLookerOpenid) | **GET** /users/{user_id}/credentials_looker_openid | Get Looker OpenId Credential
[**userCredentialsSaml**](UserApi.md#userCredentialsSaml) | **GET** /users/{user_id}/credentials_saml | Get Saml Auth Credential
[**userCredentialsTotp**](UserApi.md#userCredentialsTotp) | **GET** /users/{user_id}/credentials_totp | Get Two-Factor Credential
[**userRoles**](UserApi.md#userRoles) | **GET** /users/{user_id}/roles | Get User Roles
[**userSession**](UserApi.md#userSession) | **GET** /users/{user_id}/sessions/{session_id} | Get Web Login Session


# **allUserAccessFilters**
> \Swagger\Client\Model\AccessFilter[] allUserAccessFilters($user_id, $fields)

Get All Access Filters

### Access filter for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allUserAccessFilters($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->allUserAccessFilters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\AccessFilter[]**](../Model/AccessFilter.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allUserCredentialsApi3s**
> \Swagger\Client\Model\CredentialsApi3[] allUserCredentialsApi3s($user_id, $fields)

Get All API 3 Credentials

### API 3 login information for the specified user. This is for the newer API keys that can be added for any user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allUserCredentialsApi3s($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->allUserCredentialsApi3s: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsApi3[]**](../Model/CredentialsApi3.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allUserCredentialsEmbeds**
> \Swagger\Client\Model\CredentialsEmbed[] allUserCredentialsEmbeds($user_id, $fields)

Get All Embedding Credentials

### Embed login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allUserCredentialsEmbeds($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->allUserCredentialsEmbeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsEmbed[]**](../Model/CredentialsEmbed.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allUserSessions**
> \Swagger\Client\Model\Session[] allUserSessions($user_id, $fields)

Get All Web Login Sessions

### Web login session for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allUserSessions($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->allUserSessions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Session[]**](../Model/Session.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allUsers**
> \Swagger\Client\Model\User[] allUsers($fields, $page, $per_page, $sorts, $ids)

Get All Users

### Get information about all users.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$fields = "fields_example"; // string | Requested fields.
$page = 789; // int | Requested page.
$per_page = 789; // int | Results per page.
$sorts = "sorts_example"; // string | Fields to sort by.
$ids = array(56); // int[] | Optional list of ids to get specific users.

try {
    $result = $api_instance->allUsers($fields, $page, $per_page, $sorts, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->allUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fields. | [optional]
 **page** | **int**| Requested page. | [optional]
 **per_page** | **int**| Results per page. | [optional]
 **sorts** | **string**| Fields to sort by. | [optional]
 **ids** | [**int[]**](../Model/int.md)| Optional list of ids to get specific users. | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUser**
> \Swagger\Client\Model\User createUser($body)

Create User

### Create a user with the specified information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$body = new \Swagger\Client\Model\User(); // \Swagger\Client\Model\User | User

try {
    $result = $api_instance->createUser($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\User**](../Model/\Swagger\Client\Model\User.md)| User | [optional]

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserAccessFilter**
> \Swagger\Client\Model\AccessFilter createUserAccessFilter($user_id, $body, $fields)

Create Access Filter

### Access filter for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = new \Swagger\Client\Model\AccessFilter(); // \Swagger\Client\Model\AccessFilter | Access Filter
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createUserAccessFilter($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUserAccessFilter: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | [**\Swagger\Client\Model\AccessFilter**](../Model/\Swagger\Client\Model\AccessFilter.md)| Access Filter | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\AccessFilter**](../Model/AccessFilter.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserCredentialsApi**
> \Swagger\Client\Model\CredentialsApi createUserCredentialsApi($user_id, $body, $fields)

Create API Credential

### API login information for the specified user. This is for 'API Users' used for the 'old' query API.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = new \Swagger\Client\Model\CredentialsApi(); // \Swagger\Client\Model\CredentialsApi | API Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createUserCredentialsApi($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUserCredentialsApi: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | [**\Swagger\Client\Model\CredentialsApi**](../Model/\Swagger\Client\Model\CredentialsApi.md)| API Credential | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsApi**](../Model/CredentialsApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserCredentialsApi3**
> \Swagger\Client\Model\CredentialsApi3 createUserCredentialsApi3($user_id, $body, $fields)

Create API 3 Credential

### API 3 login information for the specified user. This is for the newer API keys that can be added for any user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = new \Swagger\Client\Model\CredentialsApi3(); // \Swagger\Client\Model\CredentialsApi3 | API 3 Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createUserCredentialsApi3($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUserCredentialsApi3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | [**\Swagger\Client\Model\CredentialsApi3**](../Model/\Swagger\Client\Model\CredentialsApi3.md)| API 3 Credential | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsApi3**](../Model/CredentialsApi3.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserCredentialsEmail**
> \Swagger\Client\Model\CredentialsEmail createUserCredentialsEmail($user_id, $body, $fields)

Create Email/Password Credential

### Email/password login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = new \Swagger\Client\Model\CredentialsEmail(); // \Swagger\Client\Model\CredentialsEmail | Email/Password Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createUserCredentialsEmail($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUserCredentialsEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | [**\Swagger\Client\Model\CredentialsEmail**](../Model/\Swagger\Client\Model\CredentialsEmail.md)| Email/Password Credential | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsEmail**](../Model/CredentialsEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserCredentialsEmailPasswordReset**
> \Swagger\Client\Model\CredentialsEmail createUserCredentialsEmailPasswordReset($user_id, $expires, $fields)

Create Password Reset Token

### Create a password reset token. This will create a cryptographically secure random password reset token for the user. If the user already has a password reset token then this invalidates the old token and creates a new one. The token is expressed as the 'password_reset_url' of the user's email/password credential object. This takes an optional 'expires' param to indicate if the new token should be an expiring token. Tokens that expire are typically used for self-service password resets for existing users. Invitation emails for new users typically are not set to expire. The expire period is always 60 minutes when expires is enabled. This method can be called with an empty body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$expires = true; // bool | Expiring token.
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createUserCredentialsEmailPasswordReset($user_id, $expires, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUserCredentialsEmailPasswordReset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **expires** | **bool**| Expiring token. | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsEmail**](../Model/CredentialsEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUserCredentialsTotp**
> \Swagger\Client\Model\CredentialsTotp createUserCredentialsTotp($user_id, $body, $fields)

Create Two-Factor Credential

### Two-factor login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = new \Swagger\Client\Model\CredentialsTotp(); // \Swagger\Client\Model\CredentialsTotp | Two-Factor Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createUserCredentialsTotp($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->createUserCredentialsTotp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | [**\Swagger\Client\Model\CredentialsTotp**](../Model/\Swagger\Client\Model\CredentialsTotp.md)| Two-Factor Credential | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsTotp**](../Model/CredentialsTotp.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUser**
> string deleteUser($user_id)

Delete User

### Delete the user with a specific id.  **DANGER** this will delete the user and all looks and other information owned by the user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user

try {
    $result = $api_instance->deleteUser($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserAccessFilter**
> string deleteUserAccessFilter($user_id, $access_filter_id)

Delete Access Filter

### Access filter for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$access_filter_id = 789; // int | id of Access Filter

try {
    $result = $api_instance->deleteUserAccessFilter($user_id, $access_filter_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserAccessFilter: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **access_filter_id** | **int**| id of Access Filter |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsApi**
> string deleteUserCredentialsApi($user_id)

Delete API Credential

### API login information for the specified user. This is for 'API Users' used for the 'old' query API.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsApi($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsApi: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsApi3**
> string deleteUserCredentialsApi3($user_id, $credentials_api3_id)

Delete API 3 Credential

### API 3 login information for the specified user. This is for the newer API keys that can be added for any user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$credentials_api3_id = 789; // int | id of API 3 Credential

try {
    $result = $api_instance->deleteUserCredentialsApi3($user_id, $credentials_api3_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsApi3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **credentials_api3_id** | **int**| id of API 3 Credential |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsEmail**
> string deleteUserCredentialsEmail($user_id)

Delete Email/Password Credential

### Email/password login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsEmail($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsEmbed**
> string deleteUserCredentialsEmbed($user_id, $credentials_embed_id)

Delete Embedding Credential

### Embed login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$credentials_embed_id = 789; // int | id of Embedding Credential

try {
    $result = $api_instance->deleteUserCredentialsEmbed($user_id, $credentials_embed_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsEmbed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **credentials_embed_id** | **int**| id of Embedding Credential |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsGoogle**
> string deleteUserCredentialsGoogle($user_id)

Delete Google Auth Credential

### Google authentication login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsGoogle($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsGoogle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsLdap**
> string deleteUserCredentialsLdap($user_id)

Delete LDAP Credential

### LDAP login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsLdap($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsLdap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsLookerOpenid**
> string deleteUserCredentialsLookerOpenid($user_id)

Delete Looker OpenId Credential

### Looker Openid login information for the specified user. Used by Looker Analysts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsLookerOpenid($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsLookerOpenid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsSaml**
> string deleteUserCredentialsSaml($user_id)

Delete Saml Auth Credential

### Saml authentication login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsSaml($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsSaml: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserCredentialsTotp**
> string deleteUserCredentialsTotp($user_id)

Delete Two-Factor Credential

### Two-factor login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user

try {
    $result = $api_instance->deleteUserCredentialsTotp($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserCredentialsTotp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserSession**
> string deleteUserSession($user_id, $session_id)

Delete Web Login Session

### Web login session for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$session_id = 789; // int | id of Web Login Session

try {
    $result = $api_instance->deleteUserSession($user_id, $session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->deleteUserSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **session_id** | **int**| id of Web Login Session |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **me**
> \Swagger\Client\Model\User me($fields)

Get Current User

### Get information about the current user; i.e. the user account currently calling the API.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->me($fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->me: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **searchUsers**
> \Swagger\Client\Model\User[] searchUsers($fields, $page, $per_page, $sorts, $id, $first_name, $last_name, $verified_looker_employee, $email, $is_disabled, $filter_or, $content_metadata_id, $group_id)

Search Users

### Search users.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$fields = "fields_example"; // string | Requested fields.
$page = 789; // int | Requested page.
$per_page = 789; // int | Results per page.
$sorts = "sorts_example"; // string | Fields to sort by.
$id = 789; // int | Match User Id.
$first_name = "first_name_example"; // string | Match First name.
$last_name = "last_name_example"; // string | Match Last name.
$verified_looker_employee = true; // bool | Match Verified Looker employee.
$email = "email_example"; // string | Match Email Address.
$is_disabled = true; // bool | Match Is disabled.
$filter_or = true; // bool | Do an OR search with parameters
$content_metadata_id = 789; // int | Id of content metadata to which users must have access
$group_id = 789; // int | Id of group of which users must be directly members

try {
    $result = $api_instance->searchUsers($fields, $page, $per_page, $sorts, $id, $first_name, $last_name, $verified_looker_employee, $email, $is_disabled, $filter_or, $content_metadata_id, $group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->searchUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fields. | [optional]
 **page** | **int**| Requested page. | [optional]
 **per_page** | **int**| Results per page. | [optional]
 **sorts** | **string**| Fields to sort by. | [optional]
 **id** | **int**| Match User Id. | [optional]
 **first_name** | **string**| Match First name. | [optional]
 **last_name** | **string**| Match Last name. | [optional]
 **verified_looker_employee** | **bool**| Match Verified Looker employee. | [optional]
 **email** | **string**| Match Email Address. | [optional]
 **is_disabled** | **bool**| Match Is disabled. | [optional]
 **filter_or** | **bool**| Do an OR search with parameters | [optional]
 **content_metadata_id** | **int**| Id of content metadata to which users must have access | [optional]
 **group_id** | **int**| Id of group of which users must be directly members | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **searchUsersNames**
> \Swagger\Client\Model\User[] searchUsersNames($pattern, $fields, $page, $per_page, $sorts, $id, $first_name, $last_name, $verified_looker_employee, $email, $is_disabled)

Search User Names

### Search users where first_name OR last_name OR email matches a string.  The results will be AND'd with any additional search parameters that are (optionally) included.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$pattern = "pattern_example"; // string | Pattern to match.
$fields = "fields_example"; // string | Requested fields.
$page = 789; // int | Requested page.
$per_page = 789; // int | Results per page.
$sorts = "sorts_example"; // string | Fields to sort by.
$id = 789; // int | Match User Id.
$first_name = "first_name_example"; // string | Match First name.
$last_name = "last_name_example"; // string | Match Last name.
$verified_looker_employee = true; // bool | Match Verified Looker employee.
$email = "email_example"; // string | Match Email Address.
$is_disabled = true; // bool | Match Is disabled.

try {
    $result = $api_instance->searchUsersNames($pattern, $fields, $page, $per_page, $sorts, $id, $first_name, $last_name, $verified_looker_employee, $email, $is_disabled);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->searchUsersNames: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pattern** | **string**| Pattern to match. |
 **fields** | **string**| Requested fields. | [optional]
 **page** | **int**| Requested page. | [optional]
 **per_page** | **int**| Results per page. | [optional]
 **sorts** | **string**| Fields to sort by. | [optional]
 **id** | **int**| Match User Id. | [optional]
 **first_name** | **string**| Match First name. | [optional]
 **last_name** | **string**| Match Last name. | [optional]
 **verified_looker_employee** | **bool**| Match Verified Looker employee. | [optional]
 **email** | **string**| Match Email Address. | [optional]
 **is_disabled** | **bool**| Match Is disabled. | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setUserRoles**
> \Swagger\Client\Model\Role[] setUserRoles($user_id, $body, $fields)

Set User Roles

### Set roles of the user with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = array(new int[]()); // int[] | array of roles ids for user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->setUserRoles($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->setUserRoles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | **int[]**| array of roles ids for user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Role[]**](../Model/Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUser**
> \Swagger\Client\Model\User updateUser($user_id, $body)

Update User

### Update information about the user with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$body = new \Swagger\Client\Model\User(); // \Swagger\Client\Model\User | User

try {
    $result = $api_instance->updateUser($user_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->updateUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **body** | [**\Swagger\Client\Model\User**](../Model/\Swagger\Client\Model\User.md)| User |

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUserAccessFilter**
> \Swagger\Client\Model\AccessFilter updateUserAccessFilter($user_id, $access_filter_id, $body, $fields)

Update Access Filter

### Access filter for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$access_filter_id = 789; // int | id of Access Filter
$body = new \Swagger\Client\Model\AccessFilter(); // \Swagger\Client\Model\AccessFilter | Access Filter
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->updateUserAccessFilter($user_id, $access_filter_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->updateUserAccessFilter: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **access_filter_id** | **int**| id of Access Filter |
 **body** | [**\Swagger\Client\Model\AccessFilter**](../Model/\Swagger\Client\Model\AccessFilter.md)| Access Filter |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\AccessFilter**](../Model/AccessFilter.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUserCredentialsEmail**
> \Swagger\Client\Model\CredentialsEmail updateUserCredentialsEmail($user_id, $body, $fields)

Update Email/Password Credential

### Email/password login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$body = new \Swagger\Client\Model\CredentialsEmail(); // \Swagger\Client\Model\CredentialsEmail | Email/Password Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->updateUserCredentialsEmail($user_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->updateUserCredentialsEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **body** | [**\Swagger\Client\Model\CredentialsEmail**](../Model/\Swagger\Client\Model\CredentialsEmail.md)| Email/Password Credential |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsEmail**](../Model/CredentialsEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **user**
> \Swagger\Client\Model\User user($user_id, $fields)

Get User by Id

### Get information about the user with a specific id.  If the caller is an admin or the caller is the user being specified, then full user information will be returned. Otherwise, a minimal 'public' variant of the user information will be returned. This contains The user name and avatar url, but no sensitive information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->user($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->user: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\User**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userAccessFilter**
> \Swagger\Client\Model\AccessFilter userAccessFilter($user_id, $access_filter_id, $fields)

Get Access Filter

### Access filter for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$access_filter_id = 789; // int | Id of Access Filter
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userAccessFilter($user_id, $access_filter_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userAccessFilter: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **access_filter_id** | **int**| Id of Access Filter |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\AccessFilter**](../Model/AccessFilter.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsApi**
> \Swagger\Client\Model\CredentialsApi userCredentialsApi($user_id, $fields)

Get API Credential

### API login information for the specified user. This is for 'API Users' used for the 'old' query API.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsApi($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsApi: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsApi**](../Model/CredentialsApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsApi3**
> \Swagger\Client\Model\CredentialsApi3 userCredentialsApi3($user_id, $credentials_api3_id, $fields)

Get API 3 Credential

### API 3 login information for the specified user. This is for the newer API keys that can be added for any user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$credentials_api3_id = 789; // int | Id of API 3 Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsApi3($user_id, $credentials_api3_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsApi3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **credentials_api3_id** | **int**| Id of API 3 Credential |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsApi3**](../Model/CredentialsApi3.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsEmail**
> \Swagger\Client\Model\CredentialsEmail userCredentialsEmail($user_id, $fields)

Get Email/Password Credential

### Email/password login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsEmail($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsEmail**](../Model/CredentialsEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsEmbed**
> \Swagger\Client\Model\CredentialsEmbed userCredentialsEmbed($user_id, $credentials_embed_id, $fields)

Get Embedding Credential

### Embed login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$credentials_embed_id = 789; // int | Id of Embedding Credential
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsEmbed($user_id, $credentials_embed_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsEmbed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **credentials_embed_id** | **int**| Id of Embedding Credential |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsEmbed**](../Model/CredentialsEmbed.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsGoogle**
> \Swagger\Client\Model\CredentialsGoogle userCredentialsGoogle($user_id, $fields)

Get Google Auth Credential

### Google authentication login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsGoogle($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsGoogle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsGoogle**](../Model/CredentialsGoogle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsLdap**
> \Swagger\Client\Model\CredentialsLDAP userCredentialsLdap($user_id, $fields)

Get LDAP Credential

### LDAP login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsLdap($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsLdap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsLDAP**](../Model/CredentialsLDAP.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsLookerOpenid**
> \Swagger\Client\Model\CredentialsLookerOpenid userCredentialsLookerOpenid($user_id, $fields)

Get Looker OpenId Credential

### Looker Openid login information for the specified user. Used by Looker Analysts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsLookerOpenid($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsLookerOpenid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsLookerOpenid**](../Model/CredentialsLookerOpenid.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsSaml**
> \Swagger\Client\Model\CredentialsSaml userCredentialsSaml($user_id, $fields)

Get Saml Auth Credential

### Saml authentication login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsSaml($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsSaml: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsSaml**](../Model/CredentialsSaml.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userCredentialsTotp**
> \Swagger\Client\Model\CredentialsTotp userCredentialsTotp($user_id, $fields)

Get Two-Factor Credential

### Two-factor login information for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userCredentialsTotp($user_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userCredentialsTotp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\CredentialsTotp**](../Model/CredentialsTotp.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userRoles**
> \Swagger\Client\Model\Role[] userRoles($user_id, $fields, $direct_association_only)

Get User Roles

### Get information about roles of the user with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | id of user
$fields = "fields_example"; // string | Requested fields.
$direct_association_only = true; // bool | Get only roles associated directly with the user: exclude those only associated through groups.

try {
    $result = $api_instance->userRoles($user_id, $fields, $direct_association_only);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userRoles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| id of user |
 **fields** | **string**| Requested fields. | [optional]
 **direct_association_only** | **bool**| Get only roles associated directly with the user: exclude those only associated through groups. | [optional]

### Return type

[**\Swagger\Client\Model\Role[]**](../Model/Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **userSession**
> \Swagger\Client\Model\Session userSession($user_id, $session_id, $fields)

Get Web Login Session

### Web login session for the specified user.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\UserApi();
$user_id = 789; // int | Id of user
$session_id = 789; // int | Id of Web Login Session
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->userSession($user_id, $session_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserApi->userSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| Id of user |
 **session_id** | **int**| Id of Web Login Session |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Session**](../Model/Session.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

