# Swagger\Client\ProjectApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**resetProjectToProduction**](ProjectApi.md#resetProjectToProduction) | **POST** /projects/{project_id}/reset_to_production | Reset To Production


# **resetProjectToProduction**
> bool resetProjectToProduction($project_id)

Reset To Production

### Reset a project with a specified ID to the version of the project that is in production.  **DANGER** this will delete any changes that have not been pushed to a remote repository.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ProjectApi();
$project_id = "project_id_example"; // string | Id of project

try {
    $result = $api_instance->resetProjectToProduction($project_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->resetProjectToProduction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **string**| Id of project |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

