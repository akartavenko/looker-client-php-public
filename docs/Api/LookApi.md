# Swagger\Client\LookApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**allLooks**](LookApi.md#allLooks) | **GET** /looks | Get All Looks
[**createLook**](LookApi.md#createLook) | **POST** /looks | Create Look
[**deleteLook**](LookApi.md#deleteLook) | **DELETE** /looks/{look_id} | Delete Look
[**look**](LookApi.md#look) | **GET** /looks/{look_id} | Get Look
[**runLook**](LookApi.md#runLook) | **GET** /looks/{look_id}/run/{result_format} | Run Look
[**updateLook**](LookApi.md#updateLook) | **PATCH** /looks/{look_id} | Update Look


# **allLooks**
> \Swagger\Client\Model\Look[] allLooks($fields)

Get All Looks

### Get all the looks.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LookApi();
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allLooks($fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LookApi->allLooks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Look[]**](../Model/Look.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createLook**
> \Swagger\Client\Model\LookWithQuery createLook($body, $fields)

Create Look

### Create a Look with specified information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LookApi();
$body = new \Swagger\Client\Model\LookWithQuery(); // \Swagger\Client\Model\LookWithQuery | Look
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->createLook($body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LookApi->createLook: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\LookWithQuery**](../Model/\Swagger\Client\Model\LookWithQuery.md)| Look | [optional]
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\LookWithQuery**](../Model/LookWithQuery.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteLook**
> string deleteLook($look_id)

Delete Look

### Delete the look with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LookApi();
$look_id = 789; // int | Id of look

try {
    $result = $api_instance->deleteLook($look_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LookApi->deleteLook: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **look_id** | **int**| Id of look |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **look**
> \Swagger\Client\Model\LookWithQuery look($look_id, $fields)

Get Look

### Get a Look.  Return detailed information about the Look and its associated Query.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LookApi();
$look_id = 789; // int | Id of look
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->look($look_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LookApi->look: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **look_id** | **int**| Id of look |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\LookWithQuery**](../Model/LookWithQuery.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **runLook**
> string runLook($look_id, $result_format, $limit, $apply_formatting, $cache, $image_width, $image_height, $generate_drill_links, $force_production, $cache_only, $path_prefix, $rebuild_pdts, $server_table_calcs)

Run Look

### Run a Look.  Runs a given look's query and returns the results in the requested format.  Suported formats:  | result_format | Description | :-----------: | :--- | | json | Plain json | json_detail | Row data plus metadata describing the fields, pivots, table calcs, and other aspects of the query | csv | Comma separated values with a header | txt | Tab separated values with a header | html | Simple html | md | Simple markdown | xlsx | MS Excel spreadsheet | sql | Returns the generated SQL rather than running the query | png | A PNG image of the visualization of the query | jpg | A JPG image of the visualization of the query

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LookApi();
$look_id = 789; // int | Id of look
$result_format = "result_format_example"; // string | Format of result
$limit = 789; // int | Row limit (may override the limit in the saved query).
$apply_formatting = true; // bool | Apply model-specified formatting to each result.
$cache = true; // bool | Get results from cache if available.
$image_width = 789; // int | Render width for image formats.
$image_height = 789; // int | Render height for image formats.
$generate_drill_links = true; // bool | Generate drill links (only applicable to 'json_detail' format.
$force_production = true; // bool | Force use of production models even if the user is in development mode.
$cache_only = true; // bool | Retrieve any results from cache even if the results have expired.
$path_prefix = "path_prefix_example"; // string | Prefix to use for drill links (url encoded).
$rebuild_pdts = true; // bool | Rebuild PDTS used in query.
$server_table_calcs = true; // bool | Perform table calculations on query results

try {
    $result = $api_instance->runLook($look_id, $result_format, $limit, $apply_formatting, $cache, $image_width, $image_height, $generate_drill_links, $force_production, $cache_only, $path_prefix, $rebuild_pdts, $server_table_calcs);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LookApi->runLook: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **look_id** | **int**| Id of look |
 **result_format** | **string**| Format of result |
 **limit** | **int**| Row limit (may override the limit in the saved query). | [optional]
 **apply_formatting** | **bool**| Apply model-specified formatting to each result. | [optional]
 **cache** | **bool**| Get results from cache if available. | [optional]
 **image_width** | **int**| Render width for image formats. | [optional]
 **image_height** | **int**| Render height for image formats. | [optional]
 **generate_drill_links** | **bool**| Generate drill links (only applicable to &#39;json_detail&#39; format. | [optional]
 **force_production** | **bool**| Force use of production models even if the user is in development mode. | [optional]
 **cache_only** | **bool**| Retrieve any results from cache even if the results have expired. | [optional]
 **path_prefix** | **string**| Prefix to use for drill links (url encoded). | [optional]
 **rebuild_pdts** | **bool**| Rebuild PDTS used in query. | [optional]
 **server_table_calcs** | **bool**| Perform table calculations on query results | [optional]

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text, application/json, image/png, image/jpg

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateLook**
> \Swagger\Client\Model\LookWithQuery updateLook($look_id, $body, $fields)

Update Look

### Update the Look with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LookApi();
$look_id = 789; // int | Id of look
$body = new \Swagger\Client\Model\LookWithQuery(); // \Swagger\Client\Model\LookWithQuery | Look
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->updateLook($look_id, $body, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LookApi->updateLook: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **look_id** | **int**| Id of look |
 **body** | [**\Swagger\Client\Model\LookWithQuery**](../Model/\Swagger\Client\Model\LookWithQuery.md)| Look |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\LookWithQuery**](../Model/LookWithQuery.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

