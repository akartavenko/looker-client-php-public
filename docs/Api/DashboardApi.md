# Swagger\Client\DashboardApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**allDashboards**](DashboardApi.md#allDashboards) | **GET** /dashboards | Get All Dashboards
[**copyDashboards**](DashboardApi.md#copyDashboards) | **POST** /dashboards/copy | Copy Dashboards to Space
[**createDashboard**](DashboardApi.md#createDashboard) | **POST** /dashboards | Create Dashboard
[**createDashboardPrefetch**](DashboardApi.md#createDashboardPrefetch) | **POST** /dashboards/{dashboard_id}/prefetch | Create Dashboard Prefetch
[**dashboard**](DashboardApi.md#dashboard) | **GET** /dashboards/{dashboard_id} | Get Dashboard
[**dashboardPrefetch**](DashboardApi.md#dashboardPrefetch) | **GET** /dashboards/{dashboard_id}/prefetch | Get Dashboard Prefetch
[**dashboardsMovePlan**](DashboardApi.md#dashboardsMovePlan) | **GET** /dashboards/move_plan | Plan For Moving Dashboards
[**deleteDashboard**](DashboardApi.md#deleteDashboard) | **DELETE** /dashboards/{dashboard_id} | Delete Dashboard
[**moveDashboards**](DashboardApi.md#moveDashboards) | **PATCH** /dashboards/move | Move Dashboards to Space
[**updateDashboard**](DashboardApi.md#updateDashboard) | **PATCH** /dashboards/{dashboard_id} | Update Dashboard


# **allDashboards**
> \Swagger\Client\Model\DashboardBase[] allDashboards($fields)

Get All Dashboards

Get information about all dashboards.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$fields = "fields_example"; // string | Requested fieds.

try {
    $result = $api_instance->allDashboards($fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->allDashboards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fieds. | [optional]

### Return type

[**\Swagger\Client\Model\DashboardBase[]**](../Model/DashboardBase.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **copyDashboards**
> \Swagger\Client\Model\Success copyDashboards($body, $space_id, $dashboard_ids)

Copy Dashboards to Space

### Copy dashboards with specified ids to space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$body = new \Swagger\Client\Model\Dashboard(); // \Swagger\Client\Model\Dashboard | Dashboard
$space_id = "space_id_example"; // string | Destination space id.
$dashboard_ids = array("dashboard_ids_example"); // string[] | Dashboard ids to copy.

try {
    $result = $api_instance->copyDashboards($body, $space_id, $dashboard_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->copyDashboards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Dashboard**](../Model/\Swagger\Client\Model\Dashboard.md)| Dashboard | [optional]
 **space_id** | **string**| Destination space id. | [optional]
 **dashboard_ids** | [**string[]**](../Model/string.md)| Dashboard ids to copy. | [optional]

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDashboard**
> \Swagger\Client\Model\Dashboard createDashboard($body)

Create Dashboard

### Create a dashboard with specified information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$body = new \Swagger\Client\Model\Dashboard(); // \Swagger\Client\Model\Dashboard | Dashboard

try {
    $result = $api_instance->createDashboard($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->createDashboard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Dashboard**](../Model/\Swagger\Client\Model\Dashboard.md)| Dashboard | [optional]

### Return type

[**\Swagger\Client\Model\Dashboard**](../Model/Dashboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDashboardPrefetch**
> \Swagger\Client\Model\PrefetchDashboardRequest createDashboardPrefetch($dashboard_id, $body)

Create Dashboard Prefetch

### Create a prefetch for a dashboard with the specified information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$dashboard_id = "dashboard_id_example"; // string | Id of dashboard
$body = new \Swagger\Client\Model\PrefetchDashboardRequest(); // \Swagger\Client\Model\PrefetchDashboardRequest | Parameters for prefetch request

try {
    $result = $api_instance->createDashboardPrefetch($dashboard_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->createDashboardPrefetch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_id** | **string**| Id of dashboard |
 **body** | [**\Swagger\Client\Model\PrefetchDashboardRequest**](../Model/\Swagger\Client\Model\PrefetchDashboardRequest.md)| Parameters for prefetch request | [optional]

### Return type

[**\Swagger\Client\Model\PrefetchDashboardRequest**](../Model/PrefetchDashboardRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dashboard**
> \Swagger\Client\Model\Dashboard dashboard($dashboard_id, $fields)

Get Dashboard

### Get information about the dashboard with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$dashboard_id = "dashboard_id_example"; // string | Id of dashboard
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->dashboard($dashboard_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->dashboard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_id** | **string**| Id of dashboard |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Dashboard**](../Model/Dashboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dashboardPrefetch**
> \Swagger\Client\Model\PrefetchMapper dashboardPrefetch($dashboard_id, $dashboard_filters)

Get Dashboard Prefetch

### Get a prefetch for a dashboard with the specified information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$dashboard_id = "dashboard_id_example"; // string | Id of dashboard
$dashboard_filters = array(new \Swagger\Client\Model\PrefetchDashboardFilterValue()); // \Swagger\Client\Model\PrefetchDashboardFilterValue[] | JSON encoded string of Dashboard filters that were applied to prefetch

try {
    $result = $api_instance->dashboardPrefetch($dashboard_id, $dashboard_filters);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->dashboardPrefetch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_id** | **string**| Id of dashboard |
 **dashboard_filters** | [**\Swagger\Client\Model\PrefetchDashboardFilterValue[]**](../Model/\Swagger\Client\Model\PrefetchDashboardFilterValue.md)| JSON encoded string of Dashboard filters that were applied to prefetch | [optional]

### Return type

[**\Swagger\Client\Model\PrefetchMapper**](../Model/PrefetchMapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dashboardsMovePlan**
> \Swagger\Client\Model\LookMovePlan dashboardsMovePlan($space_id, $dashboard_ids)

Plan For Moving Dashboards

Returns a plan describing how a set of dashboards can be moved or copied to a given space.  Dashboards cannot be moved from a space if the dashboards are currently used by other artifacts. A dashboard that cannot be moved can be copied to another space.  Use this function to determine which dashboards can be moved and which must be copied to the destination space using the move or copy API functions.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$space_id = "space_id_example"; // string | Destination space id.
$dashboard_ids = array("dashboard_ids_example"); // string[] | Dashboard ids to move.

try {
    $result = $api_instance->dashboardsMovePlan($space_id, $dashboard_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->dashboardsMovePlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Destination space id. | [optional]
 **dashboard_ids** | [**string[]**](../Model/string.md)| Dashboard ids to move. | [optional]

### Return type

[**\Swagger\Client\Model\LookMovePlan**](../Model/LookMovePlan.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDashboard**
> string deleteDashboard($dashboard_id)

Delete Dashboard

### Delete the dashboard with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$dashboard_id = "dashboard_id_example"; // string | Id of dashboard

try {
    $result = $api_instance->deleteDashboard($dashboard_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->deleteDashboard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_id** | **string**| Id of dashboard |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **moveDashboards**
> \Swagger\Client\Model\Success moveDashboards($body, $space_id, $dashboard_ids)

Move Dashboards to Space

### Move dashboards with specified ids to space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$body = new \Swagger\Client\Model\Dashboard(); // \Swagger\Client\Model\Dashboard | Dashboard
$space_id = "space_id_example"; // string | Destination space id.
$dashboard_ids = array("dashboard_ids_example"); // string[] | Dashboard ids to move.

try {
    $result = $api_instance->moveDashboards($body, $space_id, $dashboard_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->moveDashboards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Dashboard**](../Model/\Swagger\Client\Model\Dashboard.md)| Dashboard |
 **space_id** | **string**| Destination space id. | [optional]
 **dashboard_ids** | [**string[]**](../Model/string.md)| Dashboard ids to move. | [optional]

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDashboard**
> \Swagger\Client\Model\Dashboard updateDashboard($dashboard_id, $body)

Update Dashboard

### Update the dashboard with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DashboardApi();
$dashboard_id = "dashboard_id_example"; // string | Id of dashboard
$body = new \Swagger\Client\Model\Dashboard(); // \Swagger\Client\Model\Dashboard | Dashboard

try {
    $result = $api_instance->updateDashboard($dashboard_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DashboardApi->updateDashboard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboard_id** | **string**| Id of dashboard |
 **body** | [**\Swagger\Client\Model\Dashboard**](../Model/\Swagger\Client\Model\Dashboard.md)| Dashboard |

### Return type

[**\Swagger\Client\Model\Dashboard**](../Model/Dashboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

