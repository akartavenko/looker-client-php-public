# Swagger\Client\SpaceApi

All URIs are relative to **

Method | HTTP request | Description
------------- | ------------- | -------------
[**allSpaces**](SpaceApi.md#allSpaces) | **GET** /spaces | Get All Spaces
[**createSpace**](SpaceApi.md#createSpace) | **POST** /spaces | Create Space
[**deleteSpace**](SpaceApi.md#deleteSpace) | **DELETE** /spaces/{space_id} | Delete Space
[**space**](SpaceApi.md#space) | **GET** /spaces/{space_id} | Get Space
[**spaceAncestors**](SpaceApi.md#spaceAncestors) | **GET** /spaces/{space_id}/ancestors | Get Space Ancestors
[**spaceChildren**](SpaceApi.md#spaceChildren) | **GET** /spaces/{space_id}/children | Get Space Children
[**spaceChildrenSearch**](SpaceApi.md#spaceChildrenSearch) | **GET** /spaces/{space_id}/children/search | Search Space Children
[**spaceDashboards**](SpaceApi.md#spaceDashboards) | **GET** /spaces/{space_id}/dashboards | Get Space Dashboards
[**spaceLooks**](SpaceApi.md#spaceLooks) | **GET** /spaces/{space_id}/looks | Get Space Looks
[**spaceParent**](SpaceApi.md#spaceParent) | **GET** /spaces/{space_id}/parent | Get Space Parent
[**updateSpace**](SpaceApi.md#updateSpace) | **PATCH** /spaces/{space_id} | Update Space


# **allSpaces**
> \Swagger\Client\Model\SpaceBase[] allSpaces($fields)

Get All Spaces

### Get information about all spaces.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->allSpaces($fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->allSpaces: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\SpaceBase[]**](../Model/SpaceBase.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSpace**
> \Swagger\Client\Model\Space createSpace($body)

Create Space

### Create a space with specified information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$body = new \Swagger\Client\Model\Space(); // \Swagger\Client\Model\Space | Space

try {
    $result = $api_instance->createSpace($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->createSpace: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Space**](../Model/\Swagger\Client\Model\Space.md)| Space | [optional]

### Return type

[**\Swagger\Client\Model\Space**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSpace**
> string deleteSpace($space_id)

Delete Space

### Delete the space with a specific id including any children spaces. **DANGER** this will delete all looks and dashboards in the space.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space

try {
    $result = $api_instance->deleteSpace($space_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->deleteSpace: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **space**
> \Swagger\Client\Model\Space space($space_id, $fields)

Get Space

### Get information about the space with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->space($space_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->space: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Space**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **spaceAncestors**
> \Swagger\Client\Model\Space[] spaceAncestors($space_id, $fields)

Get Space Ancestors

### Get the ancestors of a space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->spaceAncestors($space_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->spaceAncestors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Space[]**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **spaceChildren**
> \Swagger\Client\Model\Space[] spaceChildren($space_id, $fields, $page, $per_page, $sorts)

Get Space Children

### Get the children of a space.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.
$page = 789; // int | Requested page.
$per_page = 789; // int | Results per page.
$sorts = "sorts_example"; // string | Fields to sort by.

try {
    $result = $api_instance->spaceChildren($space_id, $fields, $page, $per_page, $sorts);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->spaceChildren: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]
 **page** | **int**| Requested page. | [optional]
 **per_page** | **int**| Results per page. | [optional]
 **sorts** | **string**| Fields to sort by. | [optional]

### Return type

[**\Swagger\Client\Model\Space[]**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **spaceChildrenSearch**
> \Swagger\Client\Model\Space[] spaceChildrenSearch($space_id, $fields, $sorts, $name)

Search Space Children

### Search the children of a space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.
$sorts = "sorts_example"; // string | Fields to sort by.
$name = "name_example"; // string | Match Space name.

try {
    $result = $api_instance->spaceChildrenSearch($space_id, $fields, $sorts, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->spaceChildrenSearch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]
 **sorts** | **string**| Fields to sort by. | [optional]
 **name** | **string**| Match Space name. | [optional]

### Return type

[**\Swagger\Client\Model\Space[]**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **spaceDashboards**
> \Swagger\Client\Model\Dashboard[] spaceDashboards($space_id, $fields)

Get Space Dashboards

### Get the dashboards in a space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->spaceDashboards($space_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->spaceDashboards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Dashboard[]**](../Model/Dashboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **spaceLooks**
> \Swagger\Client\Model\LookWithQuery[] spaceLooks($space_id, $fields)

Get Space Looks

### Get the looks in a space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->spaceLooks($space_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->spaceLooks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\LookWithQuery[]**](../Model/LookWithQuery.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **spaceParent**
> \Swagger\Client\Model\Space spaceParent($space_id, $fields)

Get Space Parent

### Get the parent of a space

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$fields = "fields_example"; // string | Requested fields.

try {
    $result = $api_instance->spaceParent($space_id, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->spaceParent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **fields** | **string**| Requested fields. | [optional]

### Return type

[**\Swagger\Client\Model\Space**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSpace**
> \Swagger\Client\Model\Space updateSpace($space_id, $body)

Update Space

### Update the space with a specific id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SpaceApi();
$space_id = "space_id_example"; // string | Id of space
$body = new \Swagger\Client\Model\Space(); // \Swagger\Client\Model\Space | Space

try {
    $result = $api_instance->updateSpace($space_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SpaceApi->updateSpace: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **space_id** | **string**| Id of space |
 **body** | [**\Swagger\Client\Model\Space**](../Model/\Swagger\Client\Model\Space.md)| Space |

### Return type

[**\Swagger\Client\Model\Space**](../Model/Space.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

