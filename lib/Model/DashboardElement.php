<?php
/**
 * DashboardElement
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Looker API 3.0 Reference
 *
 * This document describes the Looker API.  ### Authorization keys  This API uses Looker 'API3' keys for authorization and access control. API3 keys can be created by Looker admins on the Looker admin/user edit page. Requests made to the Looker API using these keys execute in the context of / with the identity of / with the permissions of the user associated with the API3 key. Admins can create 'dummy' accounts specifically for API use, or add API3 keys to real users' accounts.  ### Client SDKs  This API uses standard RESTful practices and should be usable by any programming language capable of making HTTPS requests. Client SDKs for a variety of programming languages can be generated from the Looker API's Swagger JSON metadata to streamline use of the Looker API in your applications. A client SDK for Ruby (generated from the API Swagger JSON) is provided as an example.  ### Try It Out!  The 'api-docs' page served by the Looker instance includes 'Try it out!' buttons for each API method. Using an API3 key to login, you can call the API directly from the documentation page, to interactively explore API features and responses.  ### Versioning  Future releases of Looker will expand this API release-by-release to securely expose more and more of the core power of Looker to API client applications. API endpoints marked as \"beta\" may receive breaking changes without changing the API version number. API endpoints marked as \"stable\" may receive only non-breaking changes (new properties on response objects, new optional params in requests) without changing the API version number. To make use of new additions in your applications, you may need to regenerate your client SDK from the new API release's Swagger JSON.  This document does not cover earlier versions of the Looker API. Information about earlier versions can be found at             [Query API](http://www.looker.com/docs/reference/api-and-integration/looker-api-reference) and             [Ruby SDK](http://www.looker.com/docs/reference/api-and-integration/looker-ruby-sdk).
 *
 * OpenAPI spec version: 3.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * DashboardElement Class Doc Comment
 *
 * @category    Class */
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class DashboardElement implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'DashboardElement';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'int',
        'dashboard_id' => 'string',
        'look_id' => 'int',
        'type' => 'string',
        'listen' => 'string',
        'refresh_interval' => 'string',
        'refresh_interval_to_i' => 'int',
        'note_text' => 'string',
        'note_text_as_html' => 'string',
        'note_display' => 'string',
        'note_state' => 'string',
        'title_text' => 'string',
        'subtitle_text' => 'string',
        'body_text' => 'string',
        'body_text_as_html' => 'string',
        'look' => '\Swagger\Client\Model\LookWithQuery'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'dashboard_id' => 'dashboard_id',
        'look_id' => 'look_id',
        'type' => 'type',
        'listen' => 'listen',
        'refresh_interval' => 'refresh_interval',
        'refresh_interval_to_i' => 'refresh_interval_to_i',
        'note_text' => 'note_text',
        'note_text_as_html' => 'note_text_as_html',
        'note_display' => 'note_display',
        'note_state' => 'note_state',
        'title_text' => 'title_text',
        'subtitle_text' => 'subtitle_text',
        'body_text' => 'body_text',
        'body_text_as_html' => 'body_text_as_html',
        'look' => 'look'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'dashboard_id' => 'setDashboardId',
        'look_id' => 'setLookId',
        'type' => 'setType',
        'listen' => 'setListen',
        'refresh_interval' => 'setRefreshInterval',
        'refresh_interval_to_i' => 'setRefreshIntervalToI',
        'note_text' => 'setNoteText',
        'note_text_as_html' => 'setNoteTextAsHtml',
        'note_display' => 'setNoteDisplay',
        'note_state' => 'setNoteState',
        'title_text' => 'setTitleText',
        'subtitle_text' => 'setSubtitleText',
        'body_text' => 'setBodyText',
        'body_text_as_html' => 'setBodyTextAsHtml',
        'look' => 'setLook'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'dashboard_id' => 'getDashboardId',
        'look_id' => 'getLookId',
        'type' => 'getType',
        'listen' => 'getListen',
        'refresh_interval' => 'getRefreshInterval',
        'refresh_interval_to_i' => 'getRefreshIntervalToI',
        'note_text' => 'getNoteText',
        'note_text_as_html' => 'getNoteTextAsHtml',
        'note_display' => 'getNoteDisplay',
        'note_state' => 'getNoteState',
        'title_text' => 'getTitleText',
        'subtitle_text' => 'getSubtitleText',
        'body_text' => 'getBodyText',
        'body_text_as_html' => 'getBodyTextAsHtml',
        'look' => 'getLook'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['dashboard_id'] = isset($data['dashboard_id']) ? $data['dashboard_id'] : null;
        $this->container['look_id'] = isset($data['look_id']) ? $data['look_id'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['listen'] = isset($data['listen']) ? $data['listen'] : null;
        $this->container['refresh_interval'] = isset($data['refresh_interval']) ? $data['refresh_interval'] : null;
        $this->container['refresh_interval_to_i'] = isset($data['refresh_interval_to_i']) ? $data['refresh_interval_to_i'] : null;
        $this->container['note_text'] = isset($data['note_text']) ? $data['note_text'] : null;
        $this->container['note_text_as_html'] = isset($data['note_text_as_html']) ? $data['note_text_as_html'] : null;
        $this->container['note_display'] = isset($data['note_display']) ? $data['note_display'] : null;
        $this->container['note_state'] = isset($data['note_state']) ? $data['note_state'] : null;
        $this->container['title_text'] = isset($data['title_text']) ? $data['title_text'] : null;
        $this->container['subtitle_text'] = isset($data['subtitle_text']) ? $data['subtitle_text'] : null;
        $this->container['body_text'] = isset($data['body_text']) ? $data['body_text'] : null;
        $this->container['body_text_as_html'] = isset($data['body_text_as_html']) ? $data['body_text_as_html'] : null;
        $this->container['look'] = isset($data['look']) ? $data['look'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets id
     * @return int
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param int $id Unique Id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets dashboard_id
     * @return string
     */
    public function getDashboardId()
    {
        return $this->container['dashboard_id'];
    }

    /**
     * Sets dashboard_id
     * @param string $dashboard_id Id of Dashboard
     * @return $this
     */
    public function setDashboardId($dashboard_id)
    {
        $this->container['dashboard_id'] = $dashboard_id;

        return $this;
    }

    /**
     * Gets look_id
     * @return int
     */
    public function getLookId()
    {
        return $this->container['look_id'];
    }

    /**
     * Sets look_id
     * @param int $look_id Id Of Look
     * @return $this
     */
    public function setLookId($look_id)
    {
        $this->container['look_id'] = $look_id;

        return $this;
    }

    /**
     * Gets type
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     * @param string $type Type
     * @return $this
     */
    public function setType($type)
    {
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets listen
     * @return string
     */
    public function getListen()
    {
        return $this->container['listen'];
    }

    /**
     * Sets listen
     * @param string $listen Listen
     * @return $this
     */
    public function setListen($listen)
    {
        $this->container['listen'] = $listen;

        return $this;
    }

    /**
     * Gets refresh_interval
     * @return string
     */
    public function getRefreshInterval()
    {
        return $this->container['refresh_interval'];
    }

    /**
     * Sets refresh_interval
     * @param string $refresh_interval Refresh Interval
     * @return $this
     */
    public function setRefreshInterval($refresh_interval)
    {
        $this->container['refresh_interval'] = $refresh_interval;

        return $this;
    }

    /**
     * Gets refresh_interval_to_i
     * @return int
     */
    public function getRefreshIntervalToI()
    {
        return $this->container['refresh_interval_to_i'];
    }

    /**
     * Sets refresh_interval_to_i
     * @param int $refresh_interval_to_i Refresh Interval as integer
     * @return $this
     */
    public function setRefreshIntervalToI($refresh_interval_to_i)
    {
        $this->container['refresh_interval_to_i'] = $refresh_interval_to_i;

        return $this;
    }

    /**
     * Gets note_text
     * @return string
     */
    public function getNoteText()
    {
        return $this->container['note_text'];
    }

    /**
     * Sets note_text
     * @param string $note_text Note Text
     * @return $this
     */
    public function setNoteText($note_text)
    {
        $this->container['note_text'] = $note_text;

        return $this;
    }

    /**
     * Gets note_text_as_html
     * @return string
     */
    public function getNoteTextAsHtml()
    {
        return $this->container['note_text_as_html'];
    }

    /**
     * Sets note_text_as_html
     * @param string $note_text_as_html Note Text as Html
     * @return $this
     */
    public function setNoteTextAsHtml($note_text_as_html)
    {
        $this->container['note_text_as_html'] = $note_text_as_html;

        return $this;
    }

    /**
     * Gets note_display
     * @return string
     */
    public function getNoteDisplay()
    {
        return $this->container['note_display'];
    }

    /**
     * Sets note_display
     * @param string $note_display Note Display
     * @return $this
     */
    public function setNoteDisplay($note_display)
    {
        $this->container['note_display'] = $note_display;

        return $this;
    }

    /**
     * Gets note_state
     * @return string
     */
    public function getNoteState()
    {
        return $this->container['note_state'];
    }

    /**
     * Sets note_state
     * @param string $note_state Note State
     * @return $this
     */
    public function setNoteState($note_state)
    {
        $this->container['note_state'] = $note_state;

        return $this;
    }

    /**
     * Gets title_text
     * @return string
     */
    public function getTitleText()
    {
        return $this->container['title_text'];
    }

    /**
     * Sets title_text
     * @param string $title_text Text tile title text
     * @return $this
     */
    public function setTitleText($title_text)
    {
        $this->container['title_text'] = $title_text;

        return $this;
    }

    /**
     * Gets subtitle_text
     * @return string
     */
    public function getSubtitleText()
    {
        return $this->container['subtitle_text'];
    }

    /**
     * Sets subtitle_text
     * @param string $subtitle_text Text tile subtitle text
     * @return $this
     */
    public function setSubtitleText($subtitle_text)
    {
        $this->container['subtitle_text'] = $subtitle_text;

        return $this;
    }

    /**
     * Gets body_text
     * @return string
     */
    public function getBodyText()
    {
        return $this->container['body_text'];
    }

    /**
     * Sets body_text
     * @param string $body_text Text tile body text
     * @return $this
     */
    public function setBodyText($body_text)
    {
        $this->container['body_text'] = $body_text;

        return $this;
    }

    /**
     * Gets body_text_as_html
     * @return string
     */
    public function getBodyTextAsHtml()
    {
        return $this->container['body_text_as_html'];
    }

    /**
     * Sets body_text_as_html
     * @param string $body_text_as_html Text tile body text as Html
     * @return $this
     */
    public function setBodyTextAsHtml($body_text_as_html)
    {
        $this->container['body_text_as_html'] = $body_text_as_html;

        return $this;
    }

    /**
     * Gets look
     * @return \Swagger\Client\Model\LookWithQuery
     */
    public function getLook()
    {
        return $this->container['look'];
    }

    /**
     * Sets look
     * @param \Swagger\Client\Model\LookWithQuery $look Look
     * @return $this
     */
    public function setLook($look)
    {
        $this->container['look'] = $look;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


