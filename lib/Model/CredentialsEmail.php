<?php
/**
 * CredentialsEmail
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Looker API 3.0 Reference
 *
 * This document describes the Looker API.  ### Authorization keys  This API uses Looker 'API3' keys for authorization and access control. API3 keys can be created by Looker admins on the Looker admin/user edit page. Requests made to the Looker API using these keys execute in the context of / with the identity of / with the permissions of the user associated with the API3 key. Admins can create 'dummy' accounts specifically for API use, or add API3 keys to real users' accounts.  ### Client SDKs  This API uses standard RESTful practices and should be usable by any programming language capable of making HTTPS requests. Client SDKs for a variety of programming languages can be generated from the Looker API's Swagger JSON metadata to streamline use of the Looker API in your applications. A client SDK for Ruby (generated from the API Swagger JSON) is provided as an example.  ### Try It Out!  The 'api-docs' page served by the Looker instance includes 'Try it out!' buttons for each API method. Using an API3 key to login, you can call the API directly from the documentation page, to interactively explore API features and responses.  ### Versioning  Future releases of Looker will expand this API release-by-release to securely expose more and more of the core power of Looker to API client applications. API endpoints marked as \"beta\" may receive breaking changes without changing the API version number. API endpoints marked as \"stable\" may receive only non-breaking changes (new properties on response objects, new optional params in requests) without changing the API version number. To make use of new additions in your applications, you may need to regenerate your client SDK from the new API release's Swagger JSON.  This document does not cover earlier versions of the Looker API. Information about earlier versions can be found at             [Query API](http://www.looker.com/docs/reference/api-and-integration/looker-api-reference) and             [Ruby SDK](http://www.looker.com/docs/reference/api-and-integration/looker-ruby-sdk).
 *
 * OpenAPI spec version: 3.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * CredentialsEmail Class Doc Comment
 *
 * @category    Class */
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CredentialsEmail implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'CredentialsEmail';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'email' => 'string',
        'created_at' => 'string',
        'logged_in_at' => 'string',
        'is_disabled' => 'bool',
        'type' => 'string',
        'password_reset_url' => 'string',
        'url' => 'string',
        'user_url' => 'string'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'email' => 'email',
        'created_at' => 'created_at',
        'logged_in_at' => 'logged_in_at',
        'is_disabled' => 'is_disabled',
        'type' => 'type',
        'password_reset_url' => 'password_reset_url',
        'url' => 'url',
        'user_url' => 'user_url'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'email' => 'setEmail',
        'created_at' => 'setCreatedAt',
        'logged_in_at' => 'setLoggedInAt',
        'is_disabled' => 'setIsDisabled',
        'type' => 'setType',
        'password_reset_url' => 'setPasswordResetUrl',
        'url' => 'setUrl',
        'user_url' => 'setUserUrl'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'email' => 'getEmail',
        'created_at' => 'getCreatedAt',
        'logged_in_at' => 'getLoggedInAt',
        'is_disabled' => 'getIsDisabled',
        'type' => 'getType',
        'password_reset_url' => 'getPasswordResetUrl',
        'url' => 'getUrl',
        'user_url' => 'getUserUrl'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['email'] = isset($data['email']) ? $data['email'] : null;
        $this->container['created_at'] = isset($data['created_at']) ? $data['created_at'] : null;
        $this->container['logged_in_at'] = isset($data['logged_in_at']) ? $data['logged_in_at'] : null;
        $this->container['is_disabled'] = isset($data['is_disabled']) ? $data['is_disabled'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['password_reset_url'] = isset($data['password_reset_url']) ? $data['password_reset_url'] : null;
        $this->container['url'] = isset($data['url']) ? $data['url'] : null;
        $this->container['user_url'] = isset($data['user_url']) ? $data['user_url'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets email
     * @return string
     */
    public function getEmail()
    {
        return $this->container['email'];
    }

    /**
     * Sets email
     * @param string $email EMail address used for user login
     * @return $this
     */
    public function setEmail($email)
    {
        $this->container['email'] = $email;

        return $this;
    }

    /**
     * Gets created_at
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->container['created_at'];
    }

    /**
     * Sets created_at
     * @param string $created_at Timestamp for the creation of this credential
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->container['created_at'] = $created_at;

        return $this;
    }

    /**
     * Gets logged_in_at
     * @return string
     */
    public function getLoggedInAt()
    {
        return $this->container['logged_in_at'];
    }

    /**
     * Sets logged_in_at
     * @param string $logged_in_at Timestamp for most recent login using credential
     * @return $this
     */
    public function setLoggedInAt($logged_in_at)
    {
        $this->container['logged_in_at'] = $logged_in_at;

        return $this;
    }

    /**
     * Gets is_disabled
     * @return bool
     */
    public function getIsDisabled()
    {
        return $this->container['is_disabled'];
    }

    /**
     * Sets is_disabled
     * @param bool $is_disabled Has this credential been disabled?
     * @return $this
     */
    public function setIsDisabled($is_disabled)
    {
        $this->container['is_disabled'] = $is_disabled;

        return $this;
    }

    /**
     * Gets type
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     * @param string $type Short name for the type of this kind of credential
     * @return $this
     */
    public function setType($type)
    {
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets password_reset_url
     * @return string
     */
    public function getPasswordResetUrl()
    {
        return $this->container['password_reset_url'];
    }

    /**
     * Sets password_reset_url
     * @param string $password_reset_url Url with one-time use secret token that the user can use to reset password
     * @return $this
     */
    public function setPasswordResetUrl($password_reset_url)
    {
        $this->container['password_reset_url'] = $password_reset_url;

        return $this;
    }

    /**
     * Gets url
     * @return string
     */
    public function getUrl()
    {
        return $this->container['url'];
    }

    /**
     * Sets url
     * @param string $url Link to get this item
     * @return $this
     */
    public function setUrl($url)
    {
        $this->container['url'] = $url;

        return $this;
    }

    /**
     * Gets user_url
     * @return string
     */
    public function getUserUrl()
    {
        return $this->container['user_url'];
    }

    /**
     * Sets user_url
     * @param string $user_url Link to get this user
     * @return $this
     */
    public function setUserUrl($user_url)
    {
        $this->container['user_url'] = $user_url;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


