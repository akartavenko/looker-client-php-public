<?php
/**
 * RoleApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Looker API 3.0 Reference
 *
 * This document describes the Looker API.  ### Authorization keys  This API uses Looker 'API3' keys for authorization and access control. API3 keys can be created by Looker admins on the Looker admin/user edit page. Requests made to the Looker API using these keys execute in the context of / with the identity of / with the permissions of the user associated with the API3 key. Admins can create 'dummy' accounts specifically for API use, or add API3 keys to real users' accounts.  ### Client SDKs  This API uses standard RESTful practices and should be usable by any programming language capable of making HTTPS requests. Client SDKs for a variety of programming languages can be generated from the Looker API's Swagger JSON metadata to streamline use of the Looker API in your applications. A client SDK for Ruby (generated from the API Swagger JSON) is provided as an example.  ### Try It Out!  The 'api-docs' page served by the Looker instance includes 'Try it out!' buttons for each API method. Using an API3 key to login, you can call the API directly from the documentation page, to interactively explore API features and responses.  ### Versioning  Future releases of Looker will expand this API release-by-release to securely expose more and more of the core power of Looker to API client applications. API endpoints marked as \"beta\" may receive breaking changes without changing the API version number. API endpoints marked as \"stable\" may receive only non-breaking changes (new properties on response objects, new optional params in requests) without changing the API version number. To make use of new additions in your applications, you may need to regenerate your client SDK from the new API release's Swagger JSON.  This document does not cover earlier versions of the Looker API. Information about earlier versions can be found at             [Query API](http://www.looker.com/docs/reference/api-and-integration/looker-api-reference) and             [Ruby SDK](http://www.looker.com/docs/reference/api-and-integration/looker-ruby-sdk).
 *
 * OpenAPI spec version: 3.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiClient;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * RoleApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RoleApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test case for allModelSets
     *
     * Get All Model Sets.
     *
     */
    public function testAllModelSets()
    {

    }

    /**
     * Test case for allPermissionSets
     *
     * Get All Permission Sets.
     *
     */
    public function testAllPermissionSets()
    {

    }

    /**
     * Test case for allPermissions
     *
     * Get All Permissions.
     *
     */
    public function testAllPermissions()
    {

    }

    /**
     * Test case for allRoles
     *
     * Get All Roles.
     *
     */
    public function testAllRoles()
    {

    }

    /**
     * Test case for createModelSet
     *
     * Create Model Set.
     *
     */
    public function testCreateModelSet()
    {

    }

    /**
     * Test case for createPermissionSet
     *
     * Create Permission Set.
     *
     */
    public function testCreatePermissionSet()
    {

    }

    /**
     * Test case for createRole
     *
     * Create Role.
     *
     */
    public function testCreateRole()
    {

    }

    /**
     * Test case for deleteModelSet
     *
     * Delete Model Set.
     *
     */
    public function testDeleteModelSet()
    {

    }

    /**
     * Test case for deletePermissionSet
     *
     * Delete Permission Set.
     *
     */
    public function testDeletePermissionSet()
    {

    }

    /**
     * Test case for deleteRole
     *
     * Delete Role.
     *
     */
    public function testDeleteRole()
    {

    }

    /**
     * Test case for modelSet
     *
     * Get Model Set.
     *
     */
    public function testModelSet()
    {

    }

    /**
     * Test case for permissionSet
     *
     * Get Permission Set.
     *
     */
    public function testPermissionSet()
    {

    }

    /**
     * Test case for role
     *
     * Get Role.
     *
     */
    public function testRole()
    {

    }

    /**
     * Test case for roleGroups
     *
     * Get Role Groups.
     *
     */
    public function testRoleGroups()
    {

    }

    /**
     * Test case for roleUsers
     *
     * Get Role Users.
     *
     */
    public function testRoleUsers()
    {

    }

    /**
     * Test case for setRoleGroups
     *
     * Update Role Groups.
     *
     */
    public function testSetRoleGroups()
    {

    }

    /**
     * Test case for setRoleUsers
     *
     * Update Role Users.
     *
     */
    public function testSetRoleUsers()
    {

    }

    /**
     * Test case for updateModelSet
     *
     * Update Model Set.
     *
     */
    public function testUpdateModelSet()
    {

    }

    /**
     * Test case for updatePermissionSet
     *
     * Update Permission Set.
     *
     */
    public function testUpdatePermissionSet()
    {

    }

    /**
     * Test case for updateRole
     *
     * Update Role.
     *
     */
    public function testUpdateRole()
    {

    }

}
